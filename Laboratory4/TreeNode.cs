﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory4
{
    public class TreeNode
    {
        public string Restaurant { get; set; }
        public string Meal { get; set; }
        public string Day { get; set; }
        public string Cost { get; set; }

        public List<TreeNode> Children { get; set; }
        public TreeNode Parent { get; set; }
        public TreeNode()
        {
            Children = new List<TreeNode>();
        }

        public  TreeNode(TreeNode sample)
        {
            Restaurant = sample.Restaurant;
            Meal = sample.Meal;
            Day = sample.Day;
            Cost = sample.Cost;
            Children = new List<TreeNode>();
        }

        public override bool Equals(object obj)
        {
            if(obj.GetType() == typeof(TreeNode))
            {
                var obj1 = obj as TreeNode;
                return (
                           (obj1.Restaurant == Restaurant)
                        && (obj1.Meal == Meal)
                        && (obj1.Day == Day)
                        && (obj1.Meal == Meal)
                           );
            }
            return false;

        }
    }
}
