﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory4
{
    public class ModelItem
    {
        public string Restaurant { get; set; }
        public string Meal { get; set; }
        public string Day { get; set; }
        public string Cost { get; set; }
        public bool Reaction { get; set; }
    }
}
