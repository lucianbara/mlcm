﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Laboratory4;

namespace Laboratory4
{
    public class VersionSpaceGenerator
    {
        private List<ModelItem> InputList { get; set; }
        private TreeNode _rootGeneral;
        private TreeNode _rootParticular;
        public VersionSpaceGenerator(List<ModelItem> input)
        {
            InputList = input;
        }

        public void CalculateVersionSpace()
        {
            ModelItem item = InputList.FirstOrDefault(x => x.Reaction);
            var inputList = new List<ModelItem>(InputList);

            if(item == null)
                return;
            inputList.Remove(item);
            _rootParticular = new TreeNode()
                                  {
                                      Restaurant = item.Restaurant,
                                      Cost = item.Cost,
                                      Day = item.Day,
                                      Meal = item.Meal,
                                      Parent = null
                                  };
            _rootGeneral = new TreeNode()
                               {
                                   Restaurant = "?",
                                   Cost = "?",
                                   Day = "?",
                                   Meal = "?",
                                   Parent = null,
                               };

            _rootGeneral.Children.Add(new TreeNode(_rootGeneral) { Restaurant = item.Restaurant, Parent = _rootGeneral });          
            _rootGeneral.Children.Add(new TreeNode(_rootGeneral) {Meal = item.Meal, Parent = _rootGeneral});
            _rootGeneral.Children.Add(new TreeNode(_rootGeneral) {Day = item.Day, Parent = _rootGeneral});
            _rootGeneral.Children.Add(new TreeNode(_rootGeneral) {Cost = item.Cost, Parent = _rootGeneral});


            CalculateVersionSpaceRec(_rootParticular, _rootGeneral, inputList);
            
        }


        private void CalculateVersionSpaceRec(TreeNode rootpart, TreeNode rootgen, List<ModelItem> inputList)
        {
            TreeNode it;
            ModelItem item = inputList.FirstOrDefault();
            if(item == null)
            {
                return;
                
            }
            
            inputList.Remove(item);
            if(item.Reaction)
            {
                //positive node
                TreeNode nodepart = new TreeNode();

                //Restaurant
                if (rootpart.Restaurant == item.Restaurant)
                {
                    nodepart.Restaurant = item.Restaurant;
                }
                else
                {
                    nodepart.Restaurant = "?";

                    do
                    {
                        it = rootgen.Children.FirstOrDefault(child => child.Restaurant != "?");
                        
                       if(it!= null) 
                           rootgen.Children.Remove(it);
                    } while (it != null);
                }


                if(rootpart.Meal == item.Meal)
                {
                    nodepart.Meal = item.Meal;
                  
                }
                else
                {
                    nodepart.Meal = "?";
                    do
                    {
                        it = rootgen.Children.FirstOrDefault(child => child.Meal != "?");
                        
                       if(it!= null) 
                           rootgen.Children.Remove(it);
                    } while (it != null);
                   
                }
                       
                
                if(rootpart.Day == item.Day)
                {
                    nodepart.Day = item.Day;
                  
                }
                else
                {
                    nodepart.Day = "?";

                    do
                    {
                        it = rootgen.Children.FirstOrDefault(child => child.Day != "?");
                        
                       if(it!= null) 
                           rootgen.Children.Remove(it);
                    } while (it != null);
                }
             
                if(rootpart.Cost == item.Cost)
                {
                    nodepart.Cost = item.Cost;
               
                }
                else
                {
                    nodepart.Cost = "?";

                    do
                    {
                        it = rootgen.Children.FirstOrDefault(child => child.Cost != "?");
                        
                       if(it!= null) 
                           rootgen.Children.Remove(it);
                    } while (it != null);
                }

                nodepart.Parent = rootpart;
                rootpart.Children.Add(nodepart);
                foreach (var child in rootgen.Children)
                {
                    CalculateVersionSpaceRec(nodepart, child, inputList);
                }

            }
            //Negative example
            else
            {
                if(
                       (rootgen.Restaurant != "?" && rootgen.Restaurant != item.Restaurant)
                    && (rootgen.Meal!="?" && rootgen.Meal != item.Meal)
                    && (rootgen.Day!="?" && rootgen.Day != item.Day)
                    && (rootgen.Cost!="?" && rootgen.Cost != item.Cost)
                    )
                {
                    //Do nothing
                }
                else
                {
                    if(
                        (rootgen.Restaurant == item.Restaurant)
                      || (rootgen.Meal == item.Meal)
                      || (rootgen.Day == item.Day)
                      || (rootgen.Cost == item.Cost)
                      )
                    {
                        //Need specialization
                        TreeNode newgen = new TreeNode(rootgen) {Parent = rootgen};

                        if (newgen.Restaurant != item.Restaurant)
                        {
                            newgen.Restaurant = rootpart.Restaurant;
                        }

                        if (newgen.Meal != item.Meal)
                        {
                            newgen.Meal = rootpart.Meal;
                        }

                        if (newgen.Day != item.Day)
                        {
                            newgen.Day = rootpart.Day;
                        }

                        if (newgen.Cost != item.Cost)
                        {
                            newgen.Cost = rootpart.Cost;
                        }

                        if(!IsParticularisation(newgen, _rootGeneral))
                        {
                            
                            rootgen.Children.Add(newgen);
                        }
                        else
                        {
                            rootgen.Parent.Children.Remove(rootgen);
                        }

                    }
                }

                CalculateVersionSpaceRec(rootpart, rootgen, inputList);


            }
        }

        public String GetResult()
        {
            TreeNode result = GetResult(_rootParticular, _rootGeneral);
            if (result == null)
                return "The models did not converge";

            return "The result is [ " + result.Restaurant + ", " + result.Meal + ", " + result.Day + ", " +
                   result.Cost + "]";
        }
        private TreeNode GetResult(TreeNode rootPart, TreeNode rootGen)
        {
            TreeNode result = null;
            foreach(TreeNode child in rootGen.Children)
            {
                if (
                       child.Children.Count == 0
                    && child.Equals(rootPart)
                    )
                    result =  child;
                else
                {
                    var res = GetResult(rootPart, child);
                    if (res != null) result = res;
                }
            }

            foreach (TreeNode child in rootPart.Children)
            {
                var res = GetResult(child, rootGen);
                if (res != null)
                    result = res;
            }

            return result;

        }

        private bool IsParticularisation(TreeNode node, TreeNode root)
        {
            bool result = false;
            foreach(TreeNode child in root.Children)
            {
                if( 
                        (node.Restaurant!= "?" && child.Restaurant == node.Restaurant) 
                    &&  (child.Meal == "?" && child.Meal != node.Meal)
                    && (child.Day == "?" && child.Day != node.Day)
                    && (child.Cost == "?" && child.Cost != node.Cost)
                    )
                {
                    result = true;
                    break;
                }

                if (
                        (node.Meal != "?" && child.Meal == node.Meal)
                    && (child.Restaurant == "?" && child.Restaurant != node.Restaurant)
                    && (child.Day == "?" && child.Day != node.Day)
                    && (child.Cost == "?" && child.Cost != node.Cost)
                    )
                {
                    result = true;
                    break;
                }


               if (
                        (node.Day != "?" && child.Day == node.Day)
                    && (child.Restaurant == "?" && child.Restaurant != node.Restaurant)
                    && (child.Meal == "?" && child.Meal != node.Meal)
                    && (child.Cost == "?" && child.Cost != node.Cost)
                    )
                {
                    result = true;
                    break;
                }

               if (
                        (node.Cost != "?" && child.Cost == node.Cost)
                    && (child.Restaurant == "?" && child.Restaurant != node.Restaurant)
                    && (child.Meal == "?" && child.Meal != node.Meal) 
                    && (child.Day == "?" && child.Day != node.Day)
                    )
               {
                   result = true;
                   break;
               }
            }

            return result;
        }
    }
}
