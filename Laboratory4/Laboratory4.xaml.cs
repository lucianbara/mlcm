﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Laboratory4
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Laboratory4 : Window
    {
        public List<ModelItem> InputList;

        public void ReadFromFile(string file)
        {
            try
            {
                using (var sr = new StreamReader(file))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {

                        var parts = line.Split(',');
                        var n = new ModelItem()
                        {
                            Restaurant = parts[1].Trim(),
                            Meal = parts[2].Trim(),
                            Day = parts[3].Trim(),
                            Cost = parts[4].Trim(),
                            Reaction = (parts[5].Trim() == "yes")
                        };
                        InputList.Add(n);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error reading input");
            }
        }
        public Laboratory4()
        {
            InputList = new List<ModelItem>();
            InitializeComponent();

            ReadFromFile("input4.txt");

            InputGrid.ItemsSource = InputList;
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            VersionSpaceGenerator vs = new VersionSpaceGenerator(InputList);
            vs.CalculateVersionSpace();

            MessageBox.Show(vs.GetResult());

        }
    }
}
