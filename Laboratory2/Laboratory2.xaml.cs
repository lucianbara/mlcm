﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Laboratory2
{
    /// <summary>
    /// Interaction logic for LAboratory2.xaml
    /// </summary>
    public partial class Laboratory2 : Window
    {
        public List<KnowledgeItem> KnowledgeBase { get; set; }

        public KnowledgeItem Cup { get; set; }

        public void ReadFromFile(string file)
        {
            try
            {
                using (var sr = new StreamReader(file))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {

                        var parts = line.Split(',');
                        var elem = KnowledgeBase.FirstOrDefault(a => a.Name == parts[0].Trim());
                        if (elem == null)
                        {
                            elem = new KnowledgeItem { Name = parts[0].Trim() };
                            KnowledgeBase.Add(elem);

                        }

                        var attr = new Attribute { Name = parts[1].Trim() };

                        for (int idx = 2; idx < parts.Length; idx++)
                        {
                            attr.Description.Add(parts[idx].Trim());
                        }

                        elem.AttributeList.Add(attr);

                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error reading input");
            }
        }
        public void InitializeCup()
        {
            Cup = new KnowledgeItem
                      {
                          Name = "Cup",
                          AttributeList =
                              new List<Attribute> { new Attribute() { Name = "stable" }, new Attribute() { Name = "enables drinking" } }
                      };
        }


        public Laboratory2()
        {
            KnowledgeBase = new List<KnowledgeItem>();
            ReadFromFile("input2.txt");
            InitializeComponent();
            InitializeCup();
            KBText.Text = ShowTree();
            CupText.Text = ShowCup();


        }

        public string ShowTree()
        {
            var str = new StringBuilder();
            foreach (KnowledgeItem it in KnowledgeBase)
            {
                str.AppendLine(it.Name);

                foreach (Attribute atr in it.AttributeList)
                {
                    str.Append("      ");
                    str.Append(atr.Name);
                    str.Append(":  [ ");
                    foreach (string descr in atr.Description)
                    {

                        str.Append(" { " + descr + "} ");
                    }
                    str.AppendLine(" ]");
                }
            }

            return str.ToString();

        }

        public string ShowCup()
        {
            var str = new StringBuilder();
            str.AppendLine(Cup.Name);

            foreach (Attribute atr in Cup.AttributeList)
            {
                str.Append("      ");
                str.Append(atr.Name);
                str.Append(":  [ ");
                foreach (string descr in atr.Description)
                {

                    str.Append(" { " + descr + "} ");
                }
                str.AppendLine(" ]");
            }


            return str.ToString();
        }

        private void GetDescription_Click(object sender, RoutedEventArgs e)
        {
            foreach(Attribute at in Cup.AttributeList)
            {
                at.Description.AddRange(GetDescriptions(at.Name, KnowledgeBase));
            }

            CupText.Text = ShowCup();
        }

        private List<string> GetDescriptions(string attributeName, List<KnowledgeItem> list)
        {
            var result = new List<string>();
            foreach (KnowledgeItem it in list)
            {
                Attribute at = it.AttributeList.FirstOrDefault(a => a.Name == attributeName);
                if(at!=null)
                {
                    if (at.Description.Count > 0)
                        result.AddRange(at.Description);
                    else
                    {
                        var auxList = new List<KnowledgeItem>(KnowledgeBase);
                        auxList.Remove(it);
                        foreach(Attribute atr in it.AttributeList)
                        {
                            if (atr != at)
                            {
                                var aux = GetDescriptions(atr.Name, auxList);
                                result.AddRange(aux);
                            }
                        }   
                    }
                }
            }

            return result;
        }

    }
}
