﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory2
{
    public class KnowledgeItem
    {
        public string Name { get; set; }
        public List<Attribute> AttributeList { get; set; } 

        public KnowledgeItem()
        {
            AttributeList = new List<Attribute>();
        }
    }

    public class Attribute
    {
        public string Name { get; set; }
        public List<string> Description { get; set; }
        public Attribute()
        {
            Description = new List<string>();
        }
    }

    
}
