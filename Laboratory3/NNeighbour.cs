﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory3
{
    public class NNeighbour
    {
        private readonly List<ModelItem> _inputList;
        public int Width { get; set; }
        public int Height { get; set; }
        public NNeighbour(List<ModelItem> input, int width, int height)
        {
            _inputList = input;
            Width = width;
            Height = height;
        }

        public string Calculate()
        {
            return Calculate(_inputList, "Height");
        }

        private string Calculate(List<ModelItem> input, string property)
        {
            double avg;
            IOrderedEnumerable<ModelItem> q;
            if (input.Count == 0)
            {
                return null;
            }
            if (input.Count == 1)
            {
                return input.First().Color;
            }

            q = input.OrderBy(e => e.GetType().GetProperty(property).GetValue(e, null));
            var max = (int)q.Last().GetType().GetProperty(property).GetValue(q.Last(), null);
            var min = (int)q.First().GetType().GetProperty(property).GetValue(q.First(), null);

            avg = min + (max - min) / 2.0f;

            List<ModelItem> newInput = (avg > (int) GetType().GetProperty(property).GetValue(this, null)) ? 
                                              input.Where(a => ((int)a.GetType().GetProperty(property).GetValue(a, null) <= avg)).ToList() 
                                            : input.Where(a => ((int)a.GetType().GetProperty(property).GetValue(a, null) > avg)).ToList();

            return Calculate(newInput, property == "Width" ? "Height" : "Width");
        }
    }
}
