﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory3
{
    public class ModelItem
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public String Color { get; set; }
    }
}
