﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;

namespace Laboratory3
{
    /// <summary>
    /// Interaction logic for Laboratory3.xaml
    /// </summary>
    public partial class Laboratory3 : Window
    {

       public List<ModelItem> InputList;

        public void ReadFromFile(string file)
        {
            try
            {
                using (var sr = new StreamReader(file))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {

                        var parts = line.Split(',');
                        var n = new ModelItem()
                        {
                            Width = Int32.Parse(parts[0].Trim()),
                            Height = Int32.Parse(parts[1].Trim()),
                            Color = parts[2].Trim()
                        };
                        InputList.Add(n);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error reading input");
            }
        }

        public Laboratory3()
        {
            InputList = new List<ModelItem>();
            InitializeComponent();

            ReadFromFile("input3.txt");

            InputGrid.ItemsSource = InputList;
        }

        private void BtnCalculate_Click(object sender, RoutedEventArgs e)
        {
            int width = Int32.Parse(WidthInput.Text);
            int height = Int32.Parse(HeightInput.Text);
            NNeighbour nb = new NNeighbour(InputList, width, height);
            ColorOutput.Text = nb.Calculate();

        }
    }
}
