﻿using System;
using System.Linq;
using System.Text;
using System.Windows;

namespace Laboratory5
{
    /// <summary>
    /// Interaction logic for ListTree.xaml
    /// </summary>
    public partial class ListTree : Window
    {

        public TreeNode Root { get; set; }

        public ListTree()
        {
            InitializeComponent();
         
        }

        private void TraverseTree(StringBuilder str, TreeNode root, int level)
        {
            str.Append(NumSpaces(level));
            str.Append(root.Label);
            str.Append("\r\n");

            for (int idx = 0; idx<root.Children.Count; idx++)
            {
                str.Append(NumSpaces(level)+" ");
                str.Append(root.Attribute.Values.FirstOrDefault(i=>i.Key==idx).Value + ": \r\n");
                TraverseTree(str, root.Children[idx], level + 1);

            }
            
        }

        public void Traverse()
        {
            StringBuilder str = new StringBuilder();
            TraverseTree(str, Root, 0);
            Tree.Text = str.ToString();
        }

        private String NumSpaces(int level)
        {
            string str = "";
            for(int i=0; i<level; i++)
            {
                str += "   ";
            }
            return str;
        }


    }
    
}
