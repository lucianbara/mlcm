﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory5
{
    public class Attribute
    {
        public string Name { get; set; }
        public Dictionary<int, string> Values { get; set; }
    }


    public class TreeNode
    {

        public List<TreeNode> Children { get; set; }
        public Attribute Attribute { get; set; }
        public string Label { get; set; }

        public TreeNode()
        {
            Children = new List<TreeNode>();
        }
    }
}
