﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace Laboratory5
{
    /// <summary>
    /// Interaction logic for Laboratory5.xaml
    /// </summary>
    public partial class Laboratory5 : Window
    {
        public List<Node> InputList = new List<Node>();
        public List<Attribute> AttributeList = new List<Attribute>();

        private TreeNode _root;
        public void ReadFromFile(string file)
        {
            try
            {
                using (var sr = new StreamReader(file))
                {
                    String line;
                    int idx = 0;
                    while ((line = sr.ReadLine()) != null)
                    {

                        var parts = line.Split(',');
                        var n = new Node
                                   {
                                       Id = idx,
                                       Outlook = parts[0].Trim(),
                                       Temperature = parts[1].Trim(),
                                       Humidity = parts[2].Trim(),
                                       Windy = parts[3].Trim(),
                                       Class = (parts[4].Trim() == "P")
                                   };
                        InputList.Add(n);
                        idx++;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error reading input");
            }
        }


        public Laboratory5()
        {
            InitializeComponent();
            InitializeAttributes();
            ReadFromFile("input5.txt");

            InputGrid.ItemsSource = InputList;

            cmbOutlook.ItemsSource = AttributeList[0].Values.Values;
            cmbTemperature.ItemsSource = AttributeList[1].Values.Values;
            cmbHumidity.ItemsSource = AttributeList[2].Values.Values;
            cmbWindy.ItemsSource = AttributeList[3].Values.Values;

            cmbOutlook.SelectedIndex = 0;
            cmbTemperature.SelectedIndex = 0;
            cmbHumidity.SelectedIndex = 0;
            cmbWindy.SelectedIndex = 0;
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            if (_root == null)
            {
                _root = CreateTree(InputList, AttributeList);
            }

            Node node = new Node
                            {
                                Id = 1,
                                Outlook = cmbOutlook.SelectedItem.ToString(),
                                Humidity = cmbHumidity.SelectedItem.ToString(),
                                Temperature = cmbTemperature.SelectedItem.ToString(),
                                Windy = cmbWindy.SelectedItem.ToString()
                            };
            string result = TraverseTree(_root, node);

            MessageBox.Show("The result for the query, based on the Inputs is  " + result);

        }


        private void ViewTree_Click(object sender, RoutedEventArgs e)
        {
            if (_root == null)
            {
                _root = CreateTree(InputList, AttributeList);
            }

            var tr = new ListTree();
            tr.Root = _root;
            tr.Traverse();
            tr.Show();

        }


        #region Algorithm


        private string TraverseTree(TreeNode root, Node node)
        {
            if (root.Children.Count == 0)
                return root.Label;

            string value = node.GetType().GetProperty(root.Attribute.Name).GetValue(node, null).ToString();
            return TraverseTree(root.Children[root.Attribute.Values.FirstOrDefault(x => x.Value == value).Key], node);
        }


        public void InitializeAttributes()
        {
            //Calculate Disorders
            AttributeList.Add(new Attribute
                                  {
                                      Name = "Outlook",
                                      Values = new Dictionary<int, string>()
                                                   {
                                                       {0, "overcast"},
                                                       {1, "sunny"},
                                                       {2, "rain"}
                                                   }
                                  });

            AttributeList.Add(new Attribute
                                  {
                                      Name = "Temperature",
                                      Values = new Dictionary<int, string>()
                                                   {
                                                       {0, "cool"},
                                                       {1, "hot"},
                                                       {2, "mild"}
                                                   }
                                  });

            AttributeList.Add(new Attribute
                                  {
                                      Name = "Humidity",
                                      Values = new Dictionary<int, string>()
                                                   {
                                                       {0, "normal"},
                                                       {1, "high"}
                                                   }
                                  });

            AttributeList.Add(new Attribute
                                  {
                                      Name = "Windy",
                                      Values = new Dictionary<int, string>()
                                                   {
                                                       {0, "not"},
                                                       {1, "medium"},
                                                       {2, "very"}
                                                   }
                                  });

        }


        //ID3 algorithm
        public TreeNode CreateTree(List<Node> examples, List<Attribute> attrib)
        {
            var root = new TreeNode();

            //All examples are positive
            if (examples.Count == 0)
            {
                return null;
            }
            if (examples.Count(t => t.Class) == examples.Count)
            {
                root.Label = "P";
            }

            //All examples are negative
            else if (examples.Count(t => t.Class == false) == examples.Count)
            {
                root.Label = "N";
            }

            else
            {
                //Select Attribute
                Attribute a = SelectAttribute(examples, attrib);
                root.Attribute = a;
                root.Label = a.Name;

                foreach (String value in a.Values.Values)
                {
                    int count = examples.Count(n => n.GetType().GetProperty(a.Name).GetValue(n, null).ToString() == value);
                    if (count == 0)
                    {
                        int countYes = examples.Count(n => (n.GetType().GetProperty(a.Name).GetValue(n, null).ToString() == value && n.Class));
                        int countNo = examples.Count(n => (n.GetType().GetProperty(a.Name).GetValue(n, null).ToString() == value && !n.Class));

                        string label = countYes > countNo ? "P" : "N";
                        root.Children.Add(new TreeNode() { Label = label });
                    }
                    else
                    {
                        var newExamples =
                            examples.Where(n => n.GetType().GetProperty(a.Name).GetValue(n, null).ToString() == value).ToList();


                        var newlist = (new List<Attribute>(attrib));
                        newlist.Remove(a);
                        root.Children.Add(CreateTree(newExamples, newlist));
                    }

                }

            }
            return root;
        }


        #region GetBest Attribute
        private Attribute SelectAttribute(List<Node> examples, List<Attribute> attrib)
        {
            double minDisorder = 6.0;
            Attribute best = attrib.First();
            foreach (Attribute a in attrib)
            {
                double sum = 0;
                //Calculate Disorder

                foreach (string s in a.Values.Values)
                {
                    //Positives
                    int pos = examples.Count(n => (n.GetType().GetProperty(a.Name).GetValue(n, null).ToString() == s && n.Class));

                    //Negatives 
                    int neg = examples.Count(n => (n.GetType().GetProperty(a.Name).GetValue(n, null).ToString() == s && !n.Class));

                    double entropy = calcEntropy(pos, neg);
                    sum += (((double)pos + neg) / examples.Count) * entropy;
                }

                if (sum < minDisorder)
                {
                    minDisorder = sum;
                    best = a;
                }

            }

            return best;
        }

        private double calcEntropy(int positives, int negatives)
        {
            int total = positives + negatives;
            double ratioPositive = (double)positives / total;
            double ratioNegative = (double)negatives / total;

            if ((int)ratioPositive != 0)
                ratioPositive = -(ratioPositive) * Math.Log(ratioPositive, 2);
            if ((int)ratioNegative != 0)
                ratioNegative = -(ratioNegative) * Math.Log(ratioNegative, 2);

            double result = ratioPositive + ratioNegative;

            return result;
        }
        #endregion

        #endregion
    }
}
