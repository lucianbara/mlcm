﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory5
{
    public class Node
    {
        public int Id { get; set; }
        public string Outlook { get; set; }
        public string Temperature { get; set; }
        public string Humidity { get; set; }
        public string Windy { get; set; }
        public bool Class { get; set; }
    }
}
