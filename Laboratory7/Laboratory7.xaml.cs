﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.IO;

namespace Laboratory7
{
    /// <summary>
    /// Interaction logic for Laboratory7.xaml
    /// </summary>
    public partial class Laboratory7 : Window
    {
        public List<KnapSackItem> InputList { get; set; }

        public Laboratory7()
        {
            InputList = new List<KnapSackItem>();
            InitializeComponent();
            ReadFromFile("input7.txt");
            InputGrid.ItemsSource = InputList;
        }

        public void ReadFromFile(string file)
        {
            try
            {
                using (var sr = new StreamReader(file))
                {
                    String line;
                    int idx = 0;
                    while ((line = sr.ReadLine()) != null)
                    {

                        var parts = line.Split(',');
                        var n = new KnapSackItem
                                    {
                                        Index = idx,
                                        Name = parts[0].Trim(),
                                        Length = Int32.Parse(parts[1].Trim()),
                                        Value = Int32.Parse(parts[2].Trim())
                                    };
                        InputList.Add(n);
                        idx++;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error reading input");
            }
        }

        private void Calculate_Click(object sender, RoutedEventArgs e)
        {
            GeneticAlgorithm alg = new GeneticAlgorithm(InputList, Int32.Parse(Knapsack.Text), Int32.Parse(MaxGen.Text), Int32.Parse(MaxPop.Text));
            List<DisplayList> list =  alg.GetBestCombinations();
            OutputGrid.ItemsSource = list;
            //  KnapSackSize = Int32.Parse(Knapsack.Text);

        }
    }

    public class KnapSackItem
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public int Value { get; set; }
    }

    public class DisplayList
    {
        public string Items { get; set; }
        public int Value { get; set; }
        public int Length { get; set; }
    }
}
