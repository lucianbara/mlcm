﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory7
{
    public class GeneticAlgorithm
    {
        public int PopulationSize { get; set; }
        private List<PopulationItem> _populationVector;
        private readonly List<KnapSackItem> _inputList;
        private readonly int _knapSackSize;

        private readonly int _populationSize;
        private readonly int _maxGenerations;

        public GeneticAlgorithm(List<KnapSackItem> inputList, int knapSackSize, int maxGenerations = 20, int populationSize = 6)
        {
            _maxGenerations = maxGenerations;
            _inputList = inputList;
            _knapSackSize = knapSackSize;
            _populationSize = populationSize;
            InitPopulation();
        }

        private void InitPopulation()
        {
            var random = new Random();
            _populationVector = new List<PopulationItem>();
            for (int idx = 0; idx < _populationSize; idx++)
            {
                int vecSize;
                var item = new PopulationItem();
                do
                {
                    item.Code  = (ulong) random.Next(0, (int)Math.Pow(2, _inputList.Count) - 1);

                    int vecValue;
                    CalculateSize(item.Code, out vecSize, out vecValue);
                    item.Fitness = vecSize <= _knapSackSize ? vecValue : 0;

                }while(vecSize > _knapSackSize);

                _populationVector.Add(item);
            }

            _populationVector = _populationVector.OrderByDescending(x=>x.Fitness).ToList();

        }

        public List<DisplayList> GetBestCombinations()
        {
            GetSolutions();
            return ConvertToItemList();

        }
        private void GetSolutions()
        {
            if(_populationVector.Count < 2)
            {
                return;
            }


            for(int i = 0; i< _maxGenerations; i++)
            {

                List<PopulationItem> children = new List<PopulationItem>();

                for (int j = 1; j < _populationVector.Count; j = j + 2)
                {
                    IEnumerable<PopulationItem> items = CrossOver(_populationVector[j - 1], _populationVector[j]);

                    children.AddRange(items);
                }
                _populationVector.AddRange(children);
                _populationVector = _populationVector.OrderByDescending(x => x.Fitness).Take(_populationSize).ToList();

            }
        }


        private List<DisplayList> ConvertToItemList()
        {
            var items = new List<DisplayList>();
            foreach(PopulationItem it in _populationVector)
            {
                var item = new DisplayList();

                int size, value;
                CalculateSize(it.Code, out size, out value);
                item.Value = value;
                item.Length = size;

                item.Items = "";

                for(int i = 0; i< _inputList.Count; i++)
                {
                    if((it.Code & ((ulong)1<<i)) != 0)
                    {
                        item.Items += " " + _inputList[i].Name;
                    }
                }

                items.Add(item);
            }

            return items;
        }



        private IEnumerable<PopulationItem> CrossOver(PopulationItem parent1, PopulationItem parent2)
        {
            ulong sufix1, sufix2, prefix1, prefix2;
            int size, value;

            GetPrefixAndSuffix(parent1.Code, out prefix1, out sufix1);
            GetPrefixAndSuffix(parent2.Code, out prefix2, out sufix2);

            var children = new PopulationItem[2];

            ulong code1 = (prefix1 << (_inputList.Count / 2)) | sufix2;
            ulong code2 = (prefix2 << (_inputList.Count / 2)) | sufix1;

            code1 = Mutate(code1);

            CalculateSize(code1, out size, out value);
            if (size > _knapSackSize)
            {
                value = 0;
            }

            children[0] = new PopulationItem {Code = code1, Fitness = value};


            code2 = Mutate(code2);

            CalculateSize(code2, out size, out value);
            if (size > _knapSackSize)
            {
                value = 0;
            }

            children[1] = new PopulationItem { Code = code2, Fitness = value };

            return children;

        }

        private ulong Mutate(ulong code)
        {
            var rand = new Random();
            for(int i = 0; i< _inputList.Count; i++)
            {
                if(rand.Next(0, 10000) < 1000)     //10% chance
                {
                    code = code ^ ((ulong)1 << i);
                }
            }

            return code;
        }


        private void GetPrefixAndSuffix(ulong patten, out ulong prefix, out ulong sufix)
        {
            ulong pat = GeneratePattern(_inputList.Count) >> (_inputList.Count/2);
            sufix = patten & pat;
            prefix = ((patten - sufix) >> (_inputList.Count / 2));
        }

        private void CalculateSize(ulong binVector, out int vecSize, out int vecValue)
        {
            vecSize = 0;
            vecValue = 0;
            for (int bit = 0; bit < _inputList.Count; bit++)
            {
                if ((binVector & ((ulong)1 << bit)) != 0)
                {
                    vecSize += _inputList[bit].Length;
                    vecValue += _inputList[bit].Value;
                }
            }
        }


        private ulong GeneratePattern(int count)
        {
            ulong pattern = 0;
            for(int i = 0; i< count; i++)
            {
                pattern += (ulong) 1 << i;
            }

            return pattern;
        }
    }
    


    class PopulationItem
    {
        public ulong Code { get; set; }
        public int Fitness { get; set; }
    }


}
