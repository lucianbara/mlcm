﻿namespace Laboratory6
{
    partial class Lab6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

     #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Lab6));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxBrowse = new System.Windows.Forms.TextBox();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.labelMatchedLow = new System.Windows.Forms.Label();
            this.labelMatchedHigh = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBoxMatchedLow = new System.Windows.Forms.PictureBox();
            this.pictureBoxMatchedHigh = new System.Windows.Forms.PictureBox();
            this.pictureBoxInput = new System.Windows.Forms.PictureBox();
            this.groupBoxMatchedPattern = new System.Windows.Forms.GroupBox();
            this.buttonRecognize = new System.Windows.Forms.Button();
            this.buttonTrain = new System.Windows.Forms.Button();
            this.drawingPanel2 = new Laboratory6.DrawingPanel();
            this.drawingPanel1 = new Laboratory6.DrawingPanel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMatchedLow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMatchedHigh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxInput)).BeginInit();
            this.groupBoxMatchedPattern.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(458, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Write a simulation program in the language of your choice that implements the met" +
    "hod of training";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Select Image";
            // 
            // textBoxBrowse
            // 
            this.textBoxBrowse.Enabled = false;
            this.textBoxBrowse.Location = new System.Drawing.Point(100, 39);
            this.textBoxBrowse.Name = "textBoxBrowse";
            this.textBoxBrowse.Size = new System.Drawing.Size(248, 20);
            this.textBoxBrowse.TabIndex = 24;
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Enabled = false;
            this.buttonBrowse.Location = new System.Drawing.Point(363, 37);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(53, 23);
            this.buttonBrowse.TabIndex = 25;
            this.buttonBrowse.Text = "Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // labelMatchedLow
            // 
            this.labelMatchedLow.AutoSize = true;
            this.labelMatchedLow.Location = new System.Drawing.Point(366, 16);
            this.labelMatchedLow.Name = "labelMatchedLow";
            this.labelMatchedLow.Size = new System.Drawing.Size(75, 13);
            this.labelMatchedLow.TabIndex = 5;
            this.labelMatchedLow.Text = "Matched Low:";
            // 
            // labelMatchedHigh
            // 
            this.labelMatchedHigh.AutoSize = true;
            this.labelMatchedHigh.Location = new System.Drawing.Point(189, 16);
            this.labelMatchedHigh.Name = "labelMatchedHigh";
            this.labelMatchedHigh.Size = new System.Drawing.Size(80, 13);
            this.labelMatchedHigh.TabIndex = 4;
            this.labelMatchedHigh.Text = "Matched Hight:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Input:";
            // 
            // pictureBoxMatchedLow
            // 
            this.pictureBoxMatchedLow.Location = new System.Drawing.Point(369, 33);
            this.pictureBoxMatchedLow.Name = "pictureBoxMatchedLow";
            this.pictureBoxMatchedLow.Size = new System.Drawing.Size(63, 75);
            this.pictureBoxMatchedLow.TabIndex = 2;
            this.pictureBoxMatchedLow.TabStop = false;
            // 
            // pictureBoxMatchedHigh
            // 
            this.pictureBoxMatchedHigh.Location = new System.Drawing.Point(193, 33);
            this.pictureBoxMatchedHigh.Name = "pictureBoxMatchedHigh";
            this.pictureBoxMatchedHigh.Size = new System.Drawing.Size(63, 75);
            this.pictureBoxMatchedHigh.TabIndex = 1;
            this.pictureBoxMatchedHigh.TabStop = false;
            // 
            // pictureBoxInput
            // 
            this.pictureBoxInput.Location = new System.Drawing.Point(17, 33);
            this.pictureBoxInput.Name = "pictureBoxInput";
            this.pictureBoxInput.Size = new System.Drawing.Size(63, 75);
            this.pictureBoxInput.TabIndex = 0;
            this.pictureBoxInput.TabStop = false;
            // 
            // groupBoxMatchedPattern
            // 
            this.groupBoxMatchedPattern.Controls.Add(this.labelMatchedLow);
            this.groupBoxMatchedPattern.Controls.Add(this.labelMatchedHigh);
            this.groupBoxMatchedPattern.Controls.Add(this.label2);
            this.groupBoxMatchedPattern.Controls.Add(this.pictureBoxMatchedLow);
            this.groupBoxMatchedPattern.Controls.Add(this.pictureBoxMatchedHigh);
            this.groupBoxMatchedPattern.Controls.Add(this.pictureBoxInput);
            this.groupBoxMatchedPattern.Location = new System.Drawing.Point(17, 65);
            this.groupBoxMatchedPattern.Name = "groupBoxMatchedPattern";
            this.groupBoxMatchedPattern.Size = new System.Drawing.Size(463, 114);
            this.groupBoxMatchedPattern.TabIndex = 28;
            this.groupBoxMatchedPattern.TabStop = false;
            this.groupBoxMatchedPattern.Text = "Matched Patterns";
            // 
            // buttonRecognize
            // 
            this.buttonRecognize.Enabled = false;
            this.buttonRecognize.Location = new System.Drawing.Point(243, 185);
            this.buttonRecognize.Name = "buttonRecognize";
            this.buttonRecognize.Size = new System.Drawing.Size(86, 23);
            this.buttonRecognize.TabIndex = 27;
            this.buttonRecognize.Text = "Recognize";
            this.buttonRecognize.UseVisualStyleBackColor = true;
            this.buttonRecognize.Click += new System.EventHandler(this.buttonRecognize_Click);
            // 
            // buttonTrain
            // 
            this.buttonTrain.Location = new System.Drawing.Point(142, 185);
            this.buttonTrain.Name = "buttonTrain";
            this.buttonTrain.Size = new System.Drawing.Size(90, 23);
            this.buttonTrain.TabIndex = 26;
            this.buttonTrain.Text = "Train Network";
            this.buttonTrain.UseVisualStyleBackColor = true;
            this.buttonTrain.Click += new System.EventHandler(this.buttonTrain_Click);
            // 
            // drawingPanel2
            // 
            this.drawingPanel2.BackColor = System.Drawing.Color.White;
            this.drawingPanel2.ImageOnPanel = ((System.Drawing.Bitmap)(resources.GetObject("drawingPanel2.ImageOnPanel")));
            this.drawingPanel2.Location = new System.Drawing.Point(9, 19);
            this.drawingPanel2.Name = "drawingPanel2";
            this.drawingPanel2.PointSize = 18;
            this.drawingPanel2.Size = new System.Drawing.Size(125, 159);
            this.drawingPanel2.TabIndex = 0;
            // 
            // drawingPanel1
            // 
            this.drawingPanel1.BackColor = System.Drawing.Color.White;
            this.drawingPanel1.ImageOnPanel = ((System.Drawing.Bitmap)(resources.GetObject("drawingPanel1.ImageOnPanel")));
            this.drawingPanel1.Location = new System.Drawing.Point(9, 19);
            this.drawingPanel1.Name = "drawingPanel1";
            this.drawingPanel1.PointSize = 18;
            this.drawingPanel1.Size = new System.Drawing.Size(125, 159);
            this.drawingPanel1.TabIndex = 0;
            // 
            // Lab6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(492, 227);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxBrowse);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.groupBoxMatchedPattern);
            this.Controls.Add(this.buttonRecognize);
            this.Controls.Add(this.buttonTrain);
            this.MaximizeBox = false;
            this.Name = "Lab6";
            this.Text = "Laboratory 6 - Back Propagation";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMatchedLow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMatchedHigh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxInput)).EndInit();
            this.groupBoxMatchedPattern.ResumeLayout(false);
            this.groupBoxMatchedPattern.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DrawingPanel drawingPanel1;
        private DrawingPanel drawingPanel2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxBrowse;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.Label labelMatchedLow;
        private System.Windows.Forms.Label labelMatchedHigh;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBoxMatchedLow;
        private System.Windows.Forms.PictureBox pictureBoxMatchedHigh;
        private System.Windows.Forms.PictureBox pictureBoxInput;
        private System.Windows.Forms.GroupBox groupBoxMatchedPattern;
        private System.Windows.Forms.Button buttonRecognize;
        private System.Windows.Forms.Button buttonTrain;

    }
}