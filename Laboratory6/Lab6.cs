﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Configuration;
using Laboratory6.Lib;

namespace Laboratory6
{
    public partial class Lab6 : Form
    {


        private NeuralNetwork<string> neuralNetwork = null;

        //Data Members Required For Neural Network
        private Dictionary<string, double[]> TrainingSet = null;
        private int av_ImageHeight = 0;
        private int av_ImageWidth = 0;
        private int NumOfPatterns = 0;
        public int InputNum { get; set; }

        public string TextBoxTrainingBrowse { get; set; }

        private delegate bool TrainingCallBack();
        private readonly AsyncCallback _asyCallBack = null;
        private IAsyncResult res = null;
        private readonly ManualResetEvent _manualReset = null;

        public Lab6()
        {
            InitializeComponent();
            InitializeSettings();

            GenerateTrainingSet();
            CreateNeuralNetwork();

            _asyCallBack = new AsyncCallback(TraningCompleted);
            _manualReset = new ManualResetEvent(false);
        }


        private void buttonTrain_Click(object sender, EventArgs e)
        {
            _manualReset.Reset();

            var TR = new TrainingCallBack(neuralNetwork.Train);
            res = TR.BeginInvoke(_asyCallBack, TR);
            buttonTrain.Enabled = false;
            buttonRecognize.Enabled = false;
            timer1.Start();
        }

        private void TraningCompleted(IAsyncResult result)
        {
            if (result.AsyncState is TrainingCallBack)
            {
                TrainingCallBack TR = (TrainingCallBack)result.AsyncState;

                bool isSuccess = TR.EndInvoke(res);

                buttonTrain.Invoke(
                    (MethodInvoker)delegate
                    {
                        buttonTrain.Enabled = true;
                        buttonRecognize.Enabled = true;
                        buttonBrowse.Enabled = true;
                        textBoxBrowse.Enabled = true;
                    }
                    );

                timer1.Stop();
            }
        }


        private void buttonRecognize_Click(object sender, EventArgs e)
        {
            string MatchedHigh = "?", MatchedLow = "?";
            double OutputValueHight = 0, OutputValueLow = 0;

            double[] input = ImageProcessing.ToMatrix(drawingPanel1.ImageOnPanel,
                av_ImageHeight, av_ImageWidth);

            neuralNetwork.Recognize(input, ref MatchedHigh, ref OutputValueHight,
                ref MatchedLow, ref OutputValueLow);

            ShowRecognitionResults(MatchedHigh, MatchedLow, OutputValueHight, OutputValueLow);

        }

        private void ShowRecognitionResults(string MatchedHigh, string MatchedLow, double OutputValueHight, double OutputValueLow)
        {
            labelMatchedHigh.Text = "Hight: " + MatchedHigh + " (%" + ((int)100 * OutputValueHight).ToString("##") + ")";
            labelMatchedLow.Text = "Low: " + MatchedLow + " (%" + ((int)100 * OutputValueLow).ToString("##") + ")";

            pictureBoxInput.Image = new Bitmap(drawingPanel1.ImageOnPanel,
                pictureBoxInput.Width, pictureBoxInput.Height);

            if (MatchedHigh != "?")
                pictureBoxMatchedHigh.Image = new Bitmap(new Bitmap(TextBoxTrainingBrowse + "\\" + MatchedHigh + ".bmp"),
                    pictureBoxMatchedHigh.Width, pictureBoxMatchedHigh.Height);

            if (MatchedLow != "?")
                pictureBoxMatchedLow.Image = new Bitmap(new Bitmap(TextBoxTrainingBrowse + "\\" + MatchedLow + ".bmp"),
                    pictureBoxMatchedLow.Width, pictureBoxMatchedLow.Height);
        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog FD = new OpenFileDialog();
            FD.Filter = "Bitmap Image(*.bmp)|*.bmp";
            FD.InitialDirectory = TextBoxTrainingBrowse;

            if (FD.ShowDialog() == DialogResult.OK)
            {
                string FileName = FD.FileName;
                if (Path.GetExtension(FileName) == ".bmp")
                {
                    textBoxBrowse.Text = FileName;
                    drawingPanel1.ImageOnPanel = new Bitmap(
                        new Bitmap(FileName), drawingPanel1.Width, drawingPanel1.Height);
                }
            }
            FD.Dispose();
        }


        private void GenerateTrainingSet()
        {
            string[] Patterns = Directory.GetFiles(TextBoxTrainingBrowse, "*.bmp");

            TrainingSet = new Dictionary<string, double[]>(Patterns.Length);
            foreach (string s in Patterns)
            {
                Bitmap Temp = new Bitmap(s);
                TrainingSet.Add(Path.GetFileNameWithoutExtension(s),
                    ImageProcessing.ToMatrix(Temp, av_ImageHeight, av_ImageWidth));
                Temp.Dispose();
            }
        }

        private void InitializeSettings()
        {
            try
            {
                NameValueCollection AppSettings = ConfigurationManager.AppSettings;

                TextBoxTrainingBrowse = Path.GetFullPath("PATTERNS");

                string[] Images = Directory.GetFiles(TextBoxTrainingBrowse, "*.bmp");
                NumOfPatterns = Images.Length;

                av_ImageHeight = 0;
                av_ImageWidth = 0;

                foreach (string s in Images)
                {
                    Bitmap Temp = new Bitmap(s);
                    av_ImageHeight += Temp.Height;
                    av_ImageWidth += Temp.Width;
                    Temp.Dispose();
                }
                av_ImageHeight /= NumOfPatterns;
                av_ImageWidth /= NumOfPatterns;

                int networkInput = av_ImageHeight * av_ImageWidth;

                InputNum = ((int)((double)(networkInput + NumOfPatterns) * .33));

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Initializing Settings: " + ex.Message, "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void CreateNeuralNetwork()
        {
            if (TrainingSet == null)
                throw new Exception("Unable to Create Neural Network As There is No Data to Train..");

            neuralNetwork = new NeuralNetwork<string>
                 (new BP2Layer<string>(av_ImageHeight * av_ImageWidth, InputNum, NumOfPatterns), TrainingSet);

            //Maximum Error
            neuralNetwork.MaximumError = 1.1f;
        }

        #region Methods To Invoke UI Components If Required
        private delegate void UpdateUI(object o);
        #endregion
    }
}
