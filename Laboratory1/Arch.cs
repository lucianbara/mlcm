﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Laboratory1
{
    public class Arch
    {
        public Construct Top { get; set; }
        public Construct Left { get; set; }
        public Construct Right { get; set; }

        public Connection ConnectionLeft  { get; set; }
        public Connection ConnectionRight { get; set; }

        public ConnectionBottom ConnectionBottom { get; set; }


        public Construct GetConstructEnumFromString(string str)
        {
            switch(str)
            {
                case "Brick": 
                    return Construct.Brick;
                case "Block": 
                    return Construct.Block;
                case "Wedge":
                    return Construct.Wedge;
            }
            return Construct.Wedge;
        }

        public Connection GetConnectionEnumFromString(string str)
        {
            switch(str)
            {
                case "Support": return Connection.Support;
                case "No Support": return Connection.NoSupport;
            }
            return Connection.Support;
            
        }

        public ConnectionBottom GetConnectionBottomEnumFromString(string str)
        {
            switch (str)
            {
                case "Touch": return ConnectionBottom.Touch;
                case "No Touch": return ConnectionBottom.NoTouch;
            }
            return ConnectionBottom.Touch;

        }
    }

    public enum Construct
    {
        Brick,
        Wedge,
        Block
    }

    public enum Connection
    {
        MustNotSupport,
        NoSupport,
        Support,
        MustSupport
    }

    public enum ConnectionBottom
    {
        MustNotTouch,
        NoTouch,
        Touch,
        MustTouch
    }


    public class ConstructTreeNode
    {
        public Construct Construct { get; set; }
        public List<ConstructTreeNode> Children { get; set; }
        public ConstructTreeNode Parent { get; set; }
        public ConstructTreeNode()
        {
            Children = new List<ConstructTreeNode>();
        }

    }


}
