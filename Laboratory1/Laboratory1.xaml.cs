﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Laboratory1
{
    /// <summary>
    /// Interaction logic for Laboratory1.xaml
    /// </summary>
    public partial class Laboratory1 : Window
    {
        public static ConstructTreeNode Root { get; set; }
        private Arch _arch;
        public Laboratory1()
        {
            InitializeComponent();
            InitializeConstructTree();
            InitializeArch();


        }

        private void InitializeArch()
        {
            _arch = new Arch
                        {
                            Left = Construct.Brick,
                            Right = Construct.Brick,
                            Top = Construct.Brick,
                            ConnectionBottom = ConnectionBottom.NoTouch,
                            ConnectionLeft = Connection.Support,
                            ConnectionRight = Connection.Support
                        };


            UpdateView();
        }

        private void InitializeConstructTree()
        {
            Root = new ConstructTreeNode();
            Root.Construct = Construct.Block;
            Root.Parent = null;

            var child = new ConstructTreeNode();
            child.Construct = Construct.Brick;
            child.Parent = Root;
            Root.Children.Add(child);

            child = new ConstructTreeNode();
            child.Construct = Construct.Wedge;
            child.Parent = Root;
            Root.Children.Add(child);



        }



        private void UpdateView()
        {
            LabelTop.Content = _arch.Top.ToString();
            LabelLeft.Content = _arch.Left.ToString();
            LabelRight.Content = _arch.Right.ToString();
            LabelBottom.Content = _arch.ConnectionBottom.ToString();
            LabelConnectionLeft.Content = _arch.ConnectionLeft.ToString();
            LabelConnectionRight.Content = _arch.ConnectionRight.ToString();
        }

        private void Generalize_Click(object sender, RoutedEventArgs e)
        {
            Arch arch = GetNewArch();

            try
            {
                if (arch.Top != _arch.Top)
                {
                    _arch.Top = GeneralizeConstruct(arch.Top, _arch.Top);
                }

                if (arch.Left != _arch.Left)
                {
                    _arch.Left = GeneralizeConstruct(arch.Left, _arch.Left);
                }

                if (arch.Right != _arch.Right)
                {
                    _arch.Right = GeneralizeConstruct(arch.Right, _arch.Right);
                }


                if (arch.ConnectionBottom != _arch.ConnectionBottom)
                {
                    switch (arch.ConnectionBottom)
                    {
                        case ConnectionBottom.Touch:
                            if (_arch.ConnectionBottom == ConnectionBottom.NoTouch)
                                _arch.ConnectionBottom = ConnectionBottom.Touch;
                            if (_arch.ConnectionBottom == ConnectionBottom.MustNotTouch)
                                _arch.ConnectionBottom = ConnectionBottom.NoTouch;
                            break;
                        case ConnectionBottom.NoTouch:
                            if (_arch.ConnectionBottom == ConnectionBottom.Touch)
                                _arch.ConnectionBottom = ConnectionBottom.NoTouch;
                            if (_arch.ConnectionBottom == ConnectionBottom.MustTouch)
                                _arch.ConnectionBottom = ConnectionBottom.Touch;
                            break;
                    }
                }

                if (arch.ConnectionLeft != _arch.ConnectionLeft)
                {
                    switch (arch.ConnectionLeft)
                    {
                        case Connection.Support:
                            if (_arch.ConnectionLeft == Connection.NoSupport)
                                _arch.ConnectionLeft = Connection.Support;
                            if (_arch.ConnectionLeft == Connection.MustNotSupport)
                                _arch.ConnectionLeft = Connection.NoSupport;
                            break;
                        case Connection.NoSupport:
                            if (_arch.ConnectionLeft == Connection.Support)
                                _arch.ConnectionLeft = Connection.NoSupport;
                            if (_arch.ConnectionLeft == Connection.MustSupport)
                                _arch.ConnectionLeft = Connection.Support;
                            break;
                    }
                }

                if (arch.ConnectionRight != _arch.ConnectionRight)
                {
                    switch (arch.ConnectionRight)
                    {
                        case Connection.Support:
                            if (_arch.ConnectionRight == Connection.NoSupport)
                                _arch.ConnectionRight = Connection.Support;
                            if (_arch.ConnectionRight == Connection.MustNotSupport)
                                _arch.ConnectionRight = Connection.NoSupport;
                            break;
                        case Connection.NoSupport:
                            if (_arch.ConnectionRight == Connection.Support)
                                _arch.ConnectionRight = Connection.NoSupport;
                            if (_arch.ConnectionRight == Connection.MustSupport)
                                _arch.ConnectionRight = Connection.Support;
                            break;
                    }
                }

                UpdateView();
            }
            catch (Exception)
            {

                MessageBox.Show("Could not generalize");
            }



        }

        private Arch GetNewArch()
        {
            var arch = new Arch();
            arch.Top = arch.GetConstructEnumFromString(ComboTop.Text);
            arch.Left = arch.GetConstructEnumFromString(ComboBoxBlockLeft.Text);
            arch.Right = arch.GetConstructEnumFromString(ComboBoxBlockRight.Text);

            arch.ConnectionBottom = arch.GetConnectionBottomEnumFromString(ComboBoxInter.Text);
            arch.ConnectionLeft = arch.GetConnectionEnumFromString(ComboLeftSupport.Text);
            arch.ConnectionRight = arch.GetConnectionEnumFromString(ComboRightSupport.Text);

            return arch;
        }


        private Construct GeneralizeConstruct(Construct item1, Construct item2)
        {
            ConstructTreeNode node1 = Find(item1, Root);
            ConstructTreeNode node2 = Find(item2, Root);
            do
            {
                if (node1.Parent == null && node1.Construct == item1)
                {
                    //Keep this as parent, most generalized
                }
                else
                {
                    node1 = node1.Parent;
                }
                if (node2.Parent == null && node2.Construct == item2)
                {
                    //Keep this as parent, most generalized
                }
                else
                {
                    node2 = node2.Parent;
                }

            } while ((node1 != null && node2 != null) && (node1.Construct != node2.Construct));

            if (node1 == null || node2 == null) throw new Exception("Could not generalize");

            return node1.Construct;
        }

        private ConstructTreeNode Find(Construct item, ConstructTreeNode root)
        {
            ConstructTreeNode res = null;
            if (root.Construct == item)
                return root;
            else
            {
                foreach (ConstructTreeNode ctr in root.Children)
                {
                    var aux = Find(item, ctr);
                    if (aux != null) res = aux;
                }
            }
            return res;
        }

        private void Particularize_Click(object sender, RoutedEventArgs e)
        {
            Arch arch = GetNewArch();

            //Particularize on the example
            _arch.Top = arch.Top;
            _arch.Left = arch.Left;
            _arch.Right = arch.Right;

            switch (arch.ConnectionBottom)
            {
                case ConnectionBottom.Touch:
                    switch(_arch.ConnectionBottom)
                    {
                        case ConnectionBottom.Touch:
                            _arch.ConnectionBottom = ConnectionBottom.MustTouch;
                            break;
                       case ConnectionBottom.NoTouch:
                            _arch.ConnectionBottom = ConnectionBottom.Touch;
                            break;
                       case ConnectionBottom.MustTouch:
                            break;
                       case ConnectionBottom.MustNotTouch:
                            _arch.ConnectionBottom = ConnectionBottom.NoTouch;
                            break;
                    }
                    break;
                case ConnectionBottom.NoTouch:
                    switch (_arch.ConnectionBottom)
                    {
                        case ConnectionBottom.Touch:
                            _arch.ConnectionBottom = ConnectionBottom.NoTouch;
                            break;
                        case ConnectionBottom.NoTouch:
                            _arch.ConnectionBottom = ConnectionBottom.MustNotTouch;
                            break;
                        case ConnectionBottom.MustNotTouch:
                            break;
                        case ConnectionBottom.MustTouch:
                            _arch.ConnectionBottom = ConnectionBottom.Touch;
                            break;
                    }
                    break;
            }



            switch (arch.ConnectionLeft)
            {
                case Connection.Support:
                    switch (_arch.ConnectionLeft)
                    {
                        case Connection.Support:
                            _arch.ConnectionLeft = Connection.MustSupport;
                            break;
                        case Connection.NoSupport:
                            _arch.ConnectionLeft = Connection.Support;
                            break;
                        case Connection.MustSupport:
                            break;
                        case Connection.MustNotSupport:
                            _arch.ConnectionLeft = Connection.NoSupport;
                            break;
                    }
                    break;
                case Connection.NoSupport:
                    switch (_arch.ConnectionLeft)
                    {
                        case Connection.NoSupport:
                            _arch.ConnectionLeft = Connection.MustNotSupport;
                            break;
                        case Connection.Support:
                            _arch.ConnectionLeft = Connection.NoSupport;
                            break;
                        case Connection.MustNotSupport:
                            break;
                        case Connection.MustSupport:
                            _arch.ConnectionLeft = Connection.Support;
                            break;
                    }
                    break;
            }


            switch (arch.ConnectionRight)
            {
                case Connection.Support:
                    switch (_arch.ConnectionRight)
                    {
                        case Connection.Support:
                            _arch.ConnectionRight = Connection.MustSupport;
                            break;
                        case Connection.NoSupport:
                            _arch.ConnectionRight = Connection.Support;
                            break;
                        case Connection.MustSupport:
                            break;
                        case Connection.MustNotSupport:
                            _arch.ConnectionRight = Connection.NoSupport;
                            break;
                    }
                    break;
                case Connection.NoSupport:
                    switch (_arch.ConnectionRight)
                    {
                        case Connection.NoSupport:
                            _arch.ConnectionRight = Connection.MustNotSupport;
                            break;
                        case Connection.Support:
                            _arch.ConnectionRight = Connection.NoSupport;
                            break;
                        case Connection.MustNotSupport:
                            break;
                        case Connection.MustSupport:
                            _arch.ConnectionRight = Connection.Support;
                            break;
                    }
                    break;
            }


            UpdateView();
        }
    }
}
