﻿using System.Windows;
using Laboratory6;

namespace Laboratories
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Lab6_Click(object sender, RoutedEventArgs e)
        {
             (new  Lab6()).Show();
        }

        private void Lab5_Click(object sender, RoutedEventArgs e)
        {
            (new Laboratory5.Laboratory5()).ShowDialog();
        }

        private void Lab4_Click(object sender, RoutedEventArgs e)
        {
            (new Laboratory4.Laboratory4()).ShowDialog();
        }

        private void Lab3_Click(object sender, RoutedEventArgs e)
        {
            (new Laboratory3.Laboratory3()).ShowDialog();
        }

        private void Lab1_Click(object sender, RoutedEventArgs e)
        {
            (new Laboratory1.Laboratory1()).ShowDialog();
        }

        private void Lab2_Click(object sender, RoutedEventArgs e)
        {
            (new Laboratory2.Laboratory2()).ShowDialog();
        }

        private void Lab7_Click(object sender, RoutedEventArgs e)
        {
            (new Laboratory7.Laboratory7()).ShowDialog();
        }
    }
}
